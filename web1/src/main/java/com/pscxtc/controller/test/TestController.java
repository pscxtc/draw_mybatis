package com.pscxtc.controller.test;

import com.pscxtc.entity.UserInfo;
import com.pscxtc.service.IUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Description:
 * @version: v1.0.0
 * @author: chenxu <br/><b>e-mail:</b><br/>pscxtc@163.com
 * @date: 2019/1/11 9:18
 * <p>
 * Modification History:
 * Date           Author          Version            Description
 * ---------------------------------------------------------*
 * 2019/1/11      chenxu                     v1.0.0               初始创建
 */
@Controller
@RequestMapping("/test")
public class TestController {

    @Autowired
    @Qualifier("userInfoServiceImpl")
    private IUserInfoService userInfoService;

    @RequestMapping(value = "/t1")
    public String main(HttpServletRequest request, Map<String, Object> map) throws Exception {
        //修改 filter 后,每次登录后,都会执行
        System.out.println("/t1--121----3");
        UserInfo ui = userInfoService.queryByUsername("admin");
        System.out.println(ui.getId());
        return "/index";
    }
}

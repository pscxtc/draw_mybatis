package com.pscxtc.controller.common;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import io.swagger.annotations.Api;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * @Description:
 * @version: v1.0.0
 * @author: chenxu <br/><b>e-mail:</b><br/>pscxtc@163.com
 * @date: 2019/1/3 16:11
 * <p>
 * Modification History:
 * Date           Author          Version            Description
 * ---------------------------------------------------------*
 * 2019/1/3      chenxu                     v1.0.0               初始创建
 */
@Controller
@RequestMapping("/")
@Api(description = "登录相关")
public class LogController {

    private static final Log log = LogFactory.get();

    /**
     * 登录
     *
     * @param request
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/login")
    public String login(HttpServletRequest request, Map<String, Object> map) throws Exception {
        // 登录失败从request中获取shiro处理的异常信息。
        // shiroLoginFailure:就是shiro异常类的全类名.
        String exception = (String) request.getAttribute("shiroLoginFailure");

        log.info(exception);

        String msg = "错误信息：";
        if (exception != null) {
            if (UnknownAccountException.class.getName().equals(exception)) {
                msg += "UnknownAccountException -- > 账号不存在：";
            } else if (IncorrectCredentialsException.class.getName().equals(exception)) {
                msg += "IncorrectCredentialsException -- > 密码不正确：";
            } else if ("kaptchaValidateFailed".equals(exception)) {
                msg += "kaptchaValidateFailed -- > 验证码错误";
            } else {
                msg += "else >> " + exception;
            }
            map.put("msg", msg);
        }
        log.info(msg);
        // 此方法不处理登录成功,由shiro进行处理.
        return "/login";
    }

    /**
     * 登录成功后页面
     * @param request
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/")
    public String main(HttpServletRequest request, Map<String, Object> map) throws Exception {
        //修改 filter 后,每次登录后,都会执行
        log.info("main方法");
        return "/index";
    }

    /**
     * 登录成功后页面
     * @param request
     * @param map
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/index")
    public String index(HttpServletRequest request, Map<String, Object> map) throws Exception {
        //修改 filter 后,每次登录后,都会执行
        log.info("/index方法--登录成功");
        return "/index";
    }

}

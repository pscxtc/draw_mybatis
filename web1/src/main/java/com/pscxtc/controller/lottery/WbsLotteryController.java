package com.pscxtc.controller.lottery;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pscxtc.entity.WbsLottery;
import com.pscxtc.entity.WbsLotteryRecord;
import com.pscxtc.service.IWbsLotteryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 抽奖表 前端控制器
 * </p>
 *
 * @author com.pscxtc
 * @since 2018-12-29
 */
@RestController
@RequestMapping("/wbs-lottery")
@Api(description = "抽奖管理")
public class WbsLotteryController {

    @Autowired
    @Qualifier("wbsLotteryServiceImpl")
    private IWbsLotteryService lotteryService;


    // paramType="path" 用来解决 swagger2 SWAGGER_2 模式接口测试时,类型转换异常
    @ApiOperation(value = "分页获取所有奖品信息", notes = "分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "size", value = "条数", required = true, paramType = "path", dataType = "Long")
    })
    @RequestMapping(value="/getAllList/{pageNum}/{size}", method= RequestMethod.GET)
    public IPage<WbsLottery> getAllList(@PathVariable Long pageNum, @PathVariable Long size) {
        // 处理"/users/"的GET请求，用来获取用户列表
        // 还可以通过@RequestParam从页面中传递参数来进行查询条件或者翻页信息的传递
        IPage<WbsLottery> page = new Page<>();
        page.setPages(pageNum);
        page.setSize(size);
        return  lotteryService.page(page);
    }

    // paramType="path" 用来解决 swagger2 SWAGGER_2 模式接口测试时,类型转换异常
    @ApiOperation(value = "查询名称", notes = "")
    @ApiImplicitParam(name = "lotteryName", value = "名称", required = true, paramType = "path", dataType = "String")
    @RequestMapping(value="/findByLotteryName/{lotteryName}", method= RequestMethod.GET)
    public WbsLottery findByLotteryName(@PathVariable String lotteryName) {
        return  lotteryService.findByLotteryName(lotteryName);
    }

    // paramType="path" 用来解决 swagger2 SWAGGER_2 模式接口测试时,类型转换异常
    @ApiOperation(value = "查询ID", notes = "")
    @ApiImplicitParam(name = "id", value = "主键", required = true, paramType = "path", dataType = "Integer")
    @RequestMapping(value="/findById/{id}", method= RequestMethod.GET)
    public WbsLottery findById(@PathVariable Integer id) {
        return  lotteryService.getById(id);
    }


    @ApiOperation(value = "分页 qw ", notes = "分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "size", value = "条数", required = true, paramType = "path", dataType = "Long")
    })
    @RequestMapping(value = "/getLotteryRecord/{pageNum}/{size}", method = RequestMethod.POST)
    public IPage<WbsLottery> getLotteryRecord(@PathVariable Long pageNum, @PathVariable Long size,WbsLottery wbsLottery) {
        IPage<WbsLottery> page = new Page<>();
        page.setPages(pageNum);
        page.setSize(size);

        QueryWrapper<WbsLottery> queryWrapper = new QueryWrapper<>();
        if (StringUtils.isNotEmpty(wbsLottery.getLotteryName())){
            queryWrapper.eq("lotteryName",wbsLottery.getLotteryName());
        }
        return  lotteryService.page(page, queryWrapper);
    }

    @ApiOperation(value = "分页 自定义 ", notes = "分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "size", value = "条数", required = true, paramType = "path", dataType = "Long")
    })
    @RequestMapping(value = "/getLotteryRecordDIY/{pageNum}/{size}", method = RequestMethod.POST)
    public IPage<WbsLottery> getLotteryRecordDIY(@PathVariable Long pageNum, @PathVariable Long size,WbsLottery wbsLottery) {
        Page page = new Page((pageNum-1)*size,size);
        page.setPages(pageNum);
        page.setSize(size);

        return  lotteryService.selectPageVo(page, wbsLottery);
    }

    @ApiOperation(value="抽奖", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uId", value = "抽奖人Id", required = true, paramType = "path", dataType = "Integer"),
            @ApiImplicitParam(name = "lotteryId", value = "抽奖期数", required = true, paramType = "path", dataType = "Integer")
    })
    @RequestMapping(value="/lottery/{uId}/{lotteryId}", method= RequestMethod.GET)
    public String lottery(@PathVariable Integer uId,@PathVariable Integer lotteryId){
        String uuId = lotteryService.lottery(uId,lotteryId);
        return uuId;
    }

    @ApiOperation(value="抽奖结果查询", notes="")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uuId", value = "抽奖uuId", required = true, paramType = "path", dataType = "String"),
    })
    @RequestMapping(value="/lotteryResult/{uuId}", method= RequestMethod.GET)
    public WbsLotteryRecord lotteryResult (@PathVariable String uuId){
        WbsLotteryRecord lotteryRecord = lotteryService.queryResult(uuId);
        return lotteryRecord;
    }
}


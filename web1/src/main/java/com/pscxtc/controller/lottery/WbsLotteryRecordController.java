package com.pscxtc.controller.lottery;


import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pscxtc.entity.WbsLotteryRecord;
import com.pscxtc.entity.vo.WbsLotteryRecordVo;
import com.pscxtc.service.IWbsLotteryRecordService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 抽奖记录表 前端控制器
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-02
 */
@RestController
@RequestMapping("/wbs-lottery-record")
@Api(description = "抽奖")
public class WbsLotteryRecordController {

    private static final Log log = LogFactory.get();

    @Autowired
    @Qualifier("wbsLotteryRecordServiceImpl")
    private IWbsLotteryRecordService lotteryRecordService;

    @ApiOperation(value = "分页 自定义 ", notes = "分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "size", value = "条数", required = true, paramType = "path", dataType = "Long")
    })
    @RequestMapping(value = "/getLotteryRecordDIY/{pageNum}/{size}", method = RequestMethod.POST)
    public IPage<WbsLotteryRecord> getLotteryRecordDIY(@PathVariable Long pageNum, @PathVariable Long size, WbsLotteryRecord wbsLottery) {
        Page page = new Page(pageNum,size);
        page.setPages(pageNum);
        page.setSize(size);

        return  lotteryRecordService.selectPage(page, wbsLottery);
    }

    @ApiOperation(value = "分页 自定义 Vo", notes = "分页")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "pageNum", value = "页码", required = true, paramType = "path", dataType = "Long"),
            @ApiImplicitParam(name = "size", value = "条数", required = true, paramType = "path", dataType = "Long")
    })
    @RequestMapping(value = "/getLotteryRecordDIYVo/{pageNum}/{size}", method = RequestMethod.POST)
    public IPage<WbsLotteryRecordVo> getLotteryRecordDIYVo(@PathVariable Long pageNum, @PathVariable Long size, WbsLotteryRecord wbsLottery) {
        Page page = new Page(pageNum,size);
        page.setPages(pageNum);
        page.setSize(size);
        log.info("parameter lotteryId-log :{}",wbsLottery.getAwardId());
        return  lotteryRecordService.selectPageVo(page, wbsLottery);
    }
}

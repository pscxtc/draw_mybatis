package com.pscxtc.controller.system;


import com.pscxtc.entity.UserInfo;
import com.pscxtc.service.IUserInfoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-03
 */
@RestController
@RequestMapping("/user-info")
@Api(description = "用户管理")
public class UserInfoController {

    @Autowired
    @Qualifier("userInfoServiceImpl")
    private IUserInfoService userInfoService;

    @ApiOperation(value="用户名查询", notes="")
    @ApiImplicitParam(name = "username", value = "用户名", required = true, dataType = "String")
    @RequestMapping(value="/queryByUsername",method = RequestMethod.GET)
    public UserInfo queryByUsername (String username){
        return userInfoService.queryByUsername(username);
    }
}

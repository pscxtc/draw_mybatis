package com.pscxtc.controller.system;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-15
 */
@RestController
@RequestMapping("/resource-info")
public class ResourceInfoController {

}

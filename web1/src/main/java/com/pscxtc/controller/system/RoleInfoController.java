package com.pscxtc.controller.system;


import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.pscxtc.entity.RoleInfo;
import com.pscxtc.entity.WbsLottery;
import com.pscxtc.service.IRoleInfoService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-15
 */
@Controller
@RequestMapping("/role-info")
public class RoleInfoController {
    private static final Log log = LogFactory.get();

    @Autowired
    @Qualifier("roleInfoServiceImpl")
    private IRoleInfoService roleInfoService;

    @ApiOperation(value="页面跳转", notes="")
    @RequestMapping(value = "/add")
    public String main(HttpServletRequest request, Map<String, Object> map) throws Exception {
        log.info("/roleAdd");
        return "/roleAdd";
    }

    @ApiOperation(value = "新增角色", notes = "分页")
    @ApiImplicitParam(name = "roleInfo", value = "角色", required = true,  dataType = "WbsLotteryRecord")
    @RequestMapping(value = "/addSave", method = RequestMethod.POST)
    public String addSave( RoleInfo roleInfo) {
        roleInfoService.save(roleInfo);
        return "/index";
    }

    @ResponseBody
    @ApiOperation(value = "查询ID", notes = "")
    @ApiImplicitParam(name = "id", value = "主键", required = true, paramType = "path", dataType = "Integer")
    @RequestMapping(value="/findById/{id}", method= RequestMethod.GET)
    public RoleInfo findById(@PathVariable Integer id) {
        RoleInfo roleInfo = roleInfoService.getById(id);
        return  roleInfo;
    }
}

package com.pscxtc.shiro;

import at.pollux.thymeleaf.shiro.dialect.ShiroDialect;
import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.pscxtc.entity.RoleInfo;
import com.pscxtc.entity.UserInfo;
import com.pscxtc.service.IResourceInfoService;
import com.pscxtc.service.IRoleInfoService;
import com.pscxtc.service.IUserInfoService;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.cache.CacheManager;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.web.mgt.CookieRememberMeManager;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.servlet.SimpleCookie;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.aop.framework.autoproxy.DefaultAdvisorAutoProxyCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.Filter;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * Shiro 配置
 *
 Apache Shiro 核心通过 Filter 来实现，就好像SpringMvc 通过DispachServlet 来主控制一样。
 既然是使用 Filter 一般也就能猜到，是通过URL规则来进行过滤和权限校验，所以我们需要定义一系列关于URL的规则和访问权限。
 *
 * @version v.0.1
 */
@Configuration
public class ShiroConfiguration {


    private static final Log log = LogFactory.get();
    @Autowired
    @Qualifier("userInfoServiceImpl")
    private IUserInfoService userInfoService;
    @Autowired
    @Qualifier("roleInfoServiceImpl")
    private IRoleInfoService roleInfoService;
    @Autowired
    @Qualifier("resourceInfoServiceImpl")
    private IResourceInfoService resourceInfoService;

    //通过配置文件 进行开关
    @Value("${swagger2.isEnable}")
    private Boolean isEnable;
    @Value("${shiro.hash-algorithm:md5}")
    private String hashAlgorithm;
    @Value("${shiro.hash-iteration:2}")
    private int hashIteration;

    /**
     * ShiroFilterFactoryBean 处理拦截资源文件问题。
     * 注意：单独一个ShiroFilterFactoryBean配置是或报错的，以为在
     * 初始化ShiroFilterFactoryBean的时候需要注入：SecurityManager
     *
     Filter Chain定义说明
     1、一个URL可以配置多个Filter，使用逗号分隔
     2、当设置多个过滤器时，全部验证通过，才视为通过
     3、部分过滤器可指定参数，如perms，roles
     *
     */
    @Bean
    public ShiroFilterFactoryBean shirFilter(SecurityManager securityManager){
        ShiroFilterFactoryBean shiroFilterFactoryBean  = new ShiroFilterFactoryBean();

        // 必须设置 SecurityManager
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        //自定义拦截器
        Map<String,Filter> filters = shiroFilterFactoryBean.getFilters();
        filters.put("authc",new AdminAuthenticationFilter());

        //拦截器.
        Map<String,String> filterChainDefinitionMap = new LinkedHashMap<String,String>();

        //配置退出过滤器,其中的具体的退出代码Shiro已经替我们实现了
        filterChainDefinitionMap.put("/logout", "logout");

        //<!-- 过滤链定义，从上向下顺序执行，一般将 /**放在最为下边 -->:这是一个坑呢，一不小心代码就不好使了;
        //<!-- authc:所有url都必须认证通过才可以访问; anon:所有url都都可以匿名访问-->

        if (isEnable){
//            启用调试模式,开放swagger 相关权限
            filterChainDefinitionMap.put("/webjars/**","anon");
            filterChainDefinitionMap.put("/v2/**","anon");
            filterChainDefinitionMap.put("/swagger-ui.html","anon");
            filterChainDefinitionMap.put("/swagger-resources/**","anon");

            filterChainDefinitionMap.put("/**", "authc");
        }else {
//            非调试模式
            filterChainDefinitionMap.put("/**", "authc");
        }

        // 如果不设置默认会自动寻找Web工程根目录下的"/login.jsp"页面
        shiroFilterFactoryBean.setLoginUrl("/login");
        // 登录成功后要跳转的链接
        shiroFilterFactoryBean.setSuccessUrl("/index");
        //未授权界面;
        shiroFilterFactoryBean.setUnauthorizedUrl("/error/404");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }


    @Bean
    public SecurityManager securityManager(AuthorizingRealm myShiroRealm, CacheManager shiroRedisCacheManager, ShiroSessionDAO sessionDAO) {
        DefaultWebSessionManager sessionManager = new DefaultWebSessionManager();
        sessionManager.setSessionDAO(sessionDAO);

        DefaultWebSecurityManager securityManager = new DefaultWebSecurityManager();
        //考虑速度,暂时屏蔽 缓存
//        securityManager.setCacheManager(shiroRedisCacheManager);
//        securityManager.setRememberMeManager(cookieRememberMeManager());
//        securityManager.setSessionManager(sessionManager);
        securityManager.setRealm(myShiroRealm);
        return securityManager;
    }

    @Bean
    public AuthorizingRealm myShiroRealm(HashedCredentialsMatcher hashedCredentialsMatcher) {
        AuthorizingRealm  myShiroRealm = new AuthorizingRealm() {

            @Override
            protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
                log.info("认证 --> MyShiroRealm.doGetAuthenticationInfo()");
                //获取用户的输入的账号.
                String username = (String)token.getPrincipal();
                log.info("界面输入的用户名：{}", username);
                //通过username从数据库中查找 User对象，
                UserInfo user = userInfoService.queryByUsername(username);
                if(user == null){
                    //没有返回登录用户名对应的SimpleAuthenticationInfo对象时,就会在LoginController中抛出UnknownAccountException异常
                    return null;
                }
                SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                        user, //用户名
                        user.getPassword(), //密码
                        ByteSourceUtils.bytes(user.getCredentialsSalt()),//salt=username+salt
                        getName()  //realm name
                );
                return authenticationInfo;
            }

            @Override
            protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principal) {
                log.info("权限配置 --> MyShiroRealm.doGetAuthorizationInfo()");

                SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();
                UserInfo user  = (UserInfo)principal.getPrimaryPrincipal();
                List<RoleInfo> roles = roleInfoService.findRoleByUserId(user.getId());
                log.info("用户：{}, 角色有{}个", user.getUsername(), roles.size());
                roles.stream().forEach(
                        role -> {
                            authorizationInfo.addRole(role.getName());      // 添加角色
                            resourceInfoService.queryByRoleId(role.getId()).stream().forEach(  // 添加角色拥有的权限
                                    permission -> authorizationInfo.addStringPermission(permission.getKey())
                            );
                        }
                );
                return authorizationInfo;
            }
        };
        myShiroRealm.setCredentialsMatcher(hashedCredentialsMatcher); //设置加密规则
        myShiroRealm.setCachingEnabled(false);
        myShiroRealm.setAuthorizationCachingEnabled(false);
        //myShiroRealm.setAuthenticationCachingEnabled(true);
        return myShiroRealm;
    }

    //用于在 thymeleaf 使用shiro 标签
    @Bean
    public ShiroDialect shiroDialect() {
        return new ShiroDialect();
    }

    // 需要与存储密码时的加密规则一致
    @Bean
    public HashedCredentialsMatcher hashedCredentialsMatcher() {
        HashedCredentialsMatcher hashedCredentialsMatcher = new HashedCredentialsMatcher();
        hashedCredentialsMatcher.setHashAlgorithmName(hashAlgorithm);       //散列算法:这里使用MD5算法;
        hashedCredentialsMatcher.setHashIterations(hashIteration);          //散列的次数，比如散列两次，相当于 md5(md5(""));
        return hashedCredentialsMatcher;
    }

    /**
     * DefaultAdvisorAutoProxyCreator，Spring的一个bean，由Advisor决定对哪些类的方法进行AOP代理
     * @return
     */
    @Bean
    public DefaultAdvisorAutoProxyCreator defaultAdvisorAutoProxyCreator() {
        DefaultAdvisorAutoProxyCreator proxyCreator = new DefaultAdvisorAutoProxyCreator();
        proxyCreator.setProxyTargetClass(true);
        return proxyCreator;
    }

    /**
     *  开启shiro aop注解支持.
     *  使用代理方式;所以需要开启代码支持;
     * @param securityManager
     * @return
     */
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(SecurityManager securityManager){
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }

    @Bean
    public SimpleMappingExceptionResolver resolver() {
        SimpleMappingExceptionResolver exceptionResolver = new SimpleMappingExceptionResolver();
        Properties properties = new Properties();
        properties.setProperty("org.apache.shiro.authz.UnauthorizedException", "error/403");
        exceptionResolver.setExceptionMappings(properties);
        return exceptionResolver;
    }

    //cookie对象;
    @Bean
    public SimpleCookie rememberMeCookie() {
        log.info("ShiroConfiguration.rememberMeCookie()");
        //这个参数是cookie的名称，对应前端的checkbox的name = rememberMe
        SimpleCookie simpleCookie = new SimpleCookie("rememberMe");

        //<!-- 记住我cookie生效时间 ,单位秒;-->
        simpleCookie.setMaxAge(1800);
        return simpleCookie;
    }

    //cookie管理对象;
    @Bean
    public CookieRememberMeManager cookieRememberMeManager() {
        log.info("ShiroConfiguration.rememberMeManager()");
        CookieRememberMeManager manager = new CookieRememberMeManager();
        manager.setCookie(rememberMeCookie());
        return manager;
    }

}
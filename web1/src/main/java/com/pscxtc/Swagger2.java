package com.pscxtc;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @Description:
 * @version: v1.0.0
 * @author: chenxu <br/><b>e-mail:</b><br/>com.pscxtc@163.com
 * @date: 2018/12/18 17:34
 * <p>
 * Modification History:
 * Date           Author          Version            Description
 * ---------------------------------------------------------*
 * 2018/12/18      chenxu                     v1.0.0               初始创建
 */
@Configuration
@EnableSwagger2
public class Swagger2 {

    //通过配置文件 进行开关
    @Value("${swagger2.isEnable}")
    private Boolean isEnable;

    @Bean
    public Docket createRestApi() {
        //groupName 2.7.0 版本可以使用中文
        return new Docket(DocumentationType.SWAGGER_2).groupName("draw 测试")
                .apiInfo(apiInfo()).enable(isEnable)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.pscxtc.controller"))
                .paths(PathSelectors.any())
                .build();
    }

    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("Spring Boot 测试 RESTful APIs")
                .description(" spring boot mq mybatis 测试")
                .termsOfServiceUrl("")
                .contact("com.pscxtc")
                .version("1.0")
                .build();
    }
}

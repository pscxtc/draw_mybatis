package com.pscxtc.mapper;

import com.pscxtc.entity.WbsAward;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 奖品表 Mapper 接口
 * </p>
 *
 * @author com.pscxtc
 * @since 2019-01-02
 */
public interface WbsAwardMapper extends BaseMapper<WbsAward> {

    /**
     * 按期数 查询奖品
     * @param lotteryId 期数
     * @return  奖品结果集
     */
    List<WbsAward> findByLotteryId(@Param("lotteryId") Integer lotteryId);
}

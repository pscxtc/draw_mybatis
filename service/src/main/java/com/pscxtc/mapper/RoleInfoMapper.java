package com.pscxtc.mapper;

import com.pscxtc.entity.RoleInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-15
 */
public interface RoleInfoMapper extends BaseMapper<RoleInfo> {

    List<RoleInfo> findRoleByUserId(Integer userId);
}

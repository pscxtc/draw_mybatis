package com.pscxtc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.pscxtc.entity.WbsLotteryRecord;
import com.pscxtc.entity.vo.WbsLotteryRecordVo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 * 抽奖记录表 Mapper 接口
 * </p>
 *
 * @author com.pscxtc
 * @since 2019-01-02
 */
public interface WbsLotteryRecordMapper extends BaseMapper<WbsLotteryRecord> {

    IPage<WbsLotteryRecord> selectPage(Page page, @Param("lr") WbsLotteryRecord lr);

    IPage<WbsLotteryRecordVo> selectPageVo(Page page, @Param("lr") WbsLotteryRecord lr);

    WbsLotteryRecord findByUuId(@Param("uuId") String uuId);
}

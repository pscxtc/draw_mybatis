package com.pscxtc.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.pscxtc.entity.UserInfo;
import org.apache.ibatis.annotations.Param;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-03
 */
public interface UserInfoMapper extends BaseMapper<UserInfo> {

    UserInfo findByUsername(@Param("username") String username);
}

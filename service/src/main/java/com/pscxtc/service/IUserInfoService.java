package com.pscxtc.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.pscxtc.entity.UserInfo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-03
 */
public interface IUserInfoService extends IService<UserInfo> {

    UserInfo queryByUsername( String username);
}

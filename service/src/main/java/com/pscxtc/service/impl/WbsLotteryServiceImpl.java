package com.pscxtc.service.impl;

import cn.hutool.core.thread.ThreadUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pscxtc.client.RabbitMQClient;
import com.pscxtc.entity.WbsAward;
import com.pscxtc.entity.WbsLottery;
import com.pscxtc.entity.WbsLotteryRecord;
import com.pscxtc.mapper.WbsAwardMapper;
import com.pscxtc.mapper.WbsLotteryMapper;
import com.pscxtc.mapper.WbsLotteryRecordMapper;
import com.pscxtc.service.IWbsLotteryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 抽奖表 服务实现类
 * </p>
 *
 * @author com.pscxtc
 * @since 2018-12-29
 */
@Service
public class WbsLotteryServiceImpl extends ServiceImpl<WbsLotteryMapper, WbsLottery> implements IWbsLotteryService {

    @Autowired
    private WbsLotteryMapper lotteryMapper;
    @Autowired
    private WbsAwardMapper awardMapper;
    @Autowired
    private WbsLotteryRecordMapper lotteryRecordMapper;

    @Autowired
    private RabbitMQClient rabbitMQClient;

    @Override
    public WbsLottery findByLotteryName(String lotteryName) {
        return lotteryMapper.findByLotteryName(lotteryName);
    }

    @Override
    public IPage<WbsLottery> selectPageVo(Page page, WbsLottery lottery) {
        return lotteryMapper.selectPageVo(page,lottery);
    }

    /**
     * 抽奖
     * @return
     */
    @Override
    public String lottery(Integer uId, Integer lotteryId){
//        验证抽奖资格
        if (validateInfo(uId)){

            List<WbsAward> wbsAwards = awardMapper.findByLotteryId(lotteryId);
//          调用 mq 抽奖
            return mqLottery(wbsAwards,uId);
        }
        return null;
    }


    /**
     * 查询抽奖结果
     * @param uuId
     * @return
     */
    public WbsLotteryRecord queryResult(String uuId){
        WbsLotteryRecord lotteryRecord = null;

        //          查询抽奖结果,mq 未执行,会一直loading
        while (  null == lotteryRecord  ){
            lotteryRecord = lotteryRecordMapper.findByUuId(uuId);
            ThreadUtil.safeSleep(RandomUtil.randomInt(100,500));
        }
        return lotteryRecord;
    }

    /**
     * 验证 抽奖资格
     * @param uId
     * @return
     */
    public  boolean validateInfo(Integer uId){
        return true;
    }


    /**
     * 调用mq 抽奖
     * @param wbsAwards
     * @param uId
     * @return  返回抽奖 uuId
     */
    public String mqLottery(List<WbsAward> wbsAwards, Integer uId){
        String uuId = IdUtil.simpleUUID();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("wbsAwards",wbsAwards);
        jsonObject.put("uId",uId);
        jsonObject.put("uuId",uuId);
        rabbitMQClient.send(jsonObject.toString());
        return uuId;
    }

}

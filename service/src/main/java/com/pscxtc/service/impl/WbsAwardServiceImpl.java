package com.pscxtc.service.impl;

import com.pscxtc.entity.WbsAward;
import com.pscxtc.mapper.WbsAwardMapper;
import com.pscxtc.service.IWbsAwardService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 奖品表 服务实现类
 * </p>
 *
 * @author com.pscxtc
 * @since 2019-01-02
 */
@Service
public class WbsAwardServiceImpl extends ServiceImpl<WbsAwardMapper, WbsAward> implements IWbsAwardService {

}

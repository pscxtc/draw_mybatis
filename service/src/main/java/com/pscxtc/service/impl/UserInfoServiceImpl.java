package com.pscxtc.service.impl;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pscxtc.entity.UserInfo;
import com.pscxtc.mapper.UserInfoMapper;
import com.pscxtc.service.IUserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-03
 */
@Service
@Primary
public class UserInfoServiceImpl extends ServiceImpl<UserInfoMapper, UserInfo> implements IUserInfoService {

    private static final Log log = LogFactory.get();

    @Autowired
    private UserInfoMapper userInfoMapper;

    @Override
    public UserInfo queryByUsername(String username) {
        log.info("x1");
        return userInfoMapper.findByUsername(username);
    }
}

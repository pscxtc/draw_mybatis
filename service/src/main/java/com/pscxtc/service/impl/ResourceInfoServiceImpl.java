package com.pscxtc.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pscxtc.entity.ResourceInfo;
import com.pscxtc.mapper.ResourceInfoMapper;
import com.pscxtc.service.IResourceInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-15
 */
@Service
public class ResourceInfoServiceImpl extends ServiceImpl<ResourceInfoMapper, ResourceInfo> implements IResourceInfoService {

    @Autowired
    private ResourceInfoMapper resourceInfoMapper;

    @Override
    public List<ResourceInfo> queryByRoleId(Integer roleId) {
        return resourceInfoMapper.queryByRoleId(roleId);
    }
}

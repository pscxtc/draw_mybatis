package com.pscxtc.service.impl;

import com.pscxtc.entity.RoleInfo;
import com.pscxtc.mapper.RoleInfoMapper;
import com.pscxtc.service.IRoleInfoService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-15
 */
@Service
public class RoleInfoServiceImpl extends ServiceImpl<RoleInfoMapper, RoleInfo> implements IRoleInfoService {


    @Autowired
    private RoleInfoMapper roleInfoMapper;

    @Override
    public List<RoleInfo> findRoleByUserId(Integer userId) {
        return roleInfoMapper.findRoleByUserId(userId);
    }
}

package com.pscxtc.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.pscxtc.entity.WbsLotteryRecord;
import com.pscxtc.entity.vo.WbsLotteryRecordVo;
import com.pscxtc.mapper.WbsLotteryRecordMapper;
import com.pscxtc.service.IWbsLotteryRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 抽奖记录表 服务实现类
 * </p>
 *
 * @author com.pscxtc
 * @since 2019-01-02
 */
@Service
@Transactional
public class WbsLotteryRecordServiceImpl extends ServiceImpl<WbsLotteryRecordMapper, WbsLotteryRecord> implements IWbsLotteryRecordService {

    @Autowired
    private WbsLotteryRecordMapper lotteryRecordMapper;

    @Override
    public IPage<WbsLotteryRecord> selectPage(Page page, WbsLotteryRecord lr) {
        return lotteryRecordMapper.selectPage(page,lr);
    }

    @Override
    public IPage<WbsLotteryRecordVo> selectPageVo(Page page, WbsLotteryRecord lr) {
        return lotteryRecordMapper.selectPageVo(page,lr);
    }
}

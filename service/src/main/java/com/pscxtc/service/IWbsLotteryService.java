package com.pscxtc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pscxtc.entity.WbsLottery;
import com.pscxtc.entity.WbsLotteryRecord;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 抽奖表 服务类
 * </p>
 *
 * @author com.pscxtc
 * @since 2018-12-29
 */
public interface IWbsLotteryService extends IService<WbsLottery> {

    WbsLottery findByLotteryName( String lotteryName);

    IPage<WbsLottery> selectPageVo(Page page, WbsLottery lottery);

    //    抽奖
    String lottery (Integer uId,Integer lotteryId);

    //    查询抽将结果
    WbsLotteryRecord queryResult(String uuId);
}

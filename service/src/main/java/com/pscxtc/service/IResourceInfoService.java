package com.pscxtc.service;

import com.pscxtc.entity.ResourceInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-15
 */
public interface IResourceInfoService extends IService<ResourceInfo> {

    List<ResourceInfo> queryByRoleId(Integer roleId);
}

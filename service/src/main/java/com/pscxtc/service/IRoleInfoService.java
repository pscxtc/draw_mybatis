package com.pscxtc.service;

import com.pscxtc.entity.RoleInfo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-15
 */
public interface IRoleInfoService extends IService<RoleInfo> {

    List<RoleInfo> findRoleByUserId(Integer userId);
}

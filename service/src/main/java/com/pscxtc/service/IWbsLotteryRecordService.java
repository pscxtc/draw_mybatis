package com.pscxtc.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.pscxtc.entity.WbsLotteryRecord;
import com.pscxtc.entity.vo.WbsLotteryRecordVo;

/**
 * <p>
 * 抽奖记录表 服务类
 * </p>
 *
 * @author com.pscxtc
 * @since 2019-01-02
 */
public interface IWbsLotteryRecordService extends IService<WbsLotteryRecord> {
    IPage<WbsLotteryRecord> selectPage(Page page, WbsLotteryRecord lr);
    IPage<WbsLotteryRecordVo> selectPageVo(Page page, WbsLotteryRecord lr);
}

package com.pscxtc.service;

import com.pscxtc.entity.WbsAward;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 奖品表 服务类
 * </p>
 *
 * @author com.pscxtc
 * @since 2019-01-02
 */
public interface IWbsAwardService extends IService<WbsAward> {

}

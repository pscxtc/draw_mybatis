package com.pscxtc.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;


/**
 * @Description: BigDecimal 默认四舍五入,两位小数
 * @version: v1.0.0
 * @author: chenxu <br/><b>e-mail:</b><br/>com.pscxtc@163.com
 * @date: 2018/12/27 15:13
 * <p>
 * Modification History:
 * Date           Author          Version            Description
 * ---------------------------------------------------------*
 * 2018/12/27      chenxu                     v1.0.0               初始创建
 */
public class BigDecimaleTransfer extends JsonSerializer<BigDecimal> {
    @Override
    public void serialize(BigDecimal bigDecimal, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {

        if (bigDecimal != null) {
            jsonGenerator.writeString(bigDecimal.setScale(2, BigDecimal.ROUND_HALF_DOWN).toString());
        }
    }
}

package com.pscxtc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.sql.Timestamp;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 抽奖表
 * </p>
 *
 * @author com.pscxtc
 * @since 2019-01-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="WbsLottery对象", description="抽奖表")
public class WbsLottery implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "抽奖名称")
    @TableField("lotteryName")
    private String lotteryName;

    @ApiModelProperty(value = "每个用户抽奖次数(空为不限次数)")
    @TableField("lotteryTimes")
    private Integer lotteryTimes;

    @ApiModelProperty(value = "开始时间",hidden = true)
    @TableField("startTime")
    private Timestamp startTime;

    @ApiModelProperty(value = "结束时间",hidden = true)
    @TableField("endTime")
    private Timestamp endTime;

    @ApiModelProperty(value = "创建人")
    @TableField("createdBy")
    private Integer createdBy;

    @ApiModelProperty(value = "创建时间")
    @TableField("createTime")
    private Timestamp createTime;

    @ApiModelProperty(value = "修改人")
    @TableField("updatedBy")
    private Integer updatedBy;

    @ApiModelProperty(value = "修改时间")
    @TableField("updateTime")
    private Timestamp updateTime;

    @ApiModelProperty(value = "是否删除")
    @TableLogic
    private Integer state;


}

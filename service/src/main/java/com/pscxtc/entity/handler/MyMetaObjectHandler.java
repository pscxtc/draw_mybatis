package com.pscxtc.entity.handler;

import cn.hutool.log.Log;
import cn.hutool.log.LogFactory;
import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.pscxtc.entity.UserInfo;
import com.pscxtc.entity.enums.StatusEnum;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.shiro.SecurityUtils;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

/**
 * @Description:
 * @version: v1.0.0
 * @author: chenxu <br/><b>e-mail:</b><br/>pscxtc@163.com
 * @date: 2019/1/15 10:40
 * <p>
 * Modification History:
 * Date           Author          Version            Description
 * ---------------------------------------------------------*
 * 2019/1/15      chenxu                     v1.0.0               初始创建
 */
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {
    private static final Log LOGGER = LogFactory.get();

    @Override
    public void insertFill(MetaObject metaObject) {
        LOGGER.info("start insert fill ....");
        //避免使用metaObject.setValue()
        this.setFieldValByName("createDate", new Timestamp(System.currentTimeMillis()), metaObject);
        //获得当前登录人
        this.setFieldValByName("creator",((UserInfo) SecurityUtils.getSubject().getPrincipal()).getId(), metaObject);
        this.setFieldValByName("state", StatusEnum.NORMAL.getValue(), metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        LOGGER.info("start update fill ....");
        this.setFieldValByName("operator", "Tom", metaObject);
    }
}

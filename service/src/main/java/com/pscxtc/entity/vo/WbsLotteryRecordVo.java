package com.pscxtc.entity.vo;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pscxtc.entity.WbsLotteryRecord;
import com.pscxtc.utils.BigDecimaleTransfer;
import lombok.Data;

import java.math.BigDecimal;

/**
 * @Description: 采用继承方式,可满足父对象json 转换定义,本身也可 采用json 格式转化
 * @version: v1.0.0
 * @author: chenxu <br/><b>e-mail:</b><br/>pscxtc@163.com
 * @date: 2019/1/2 15:01
 * <p>
 * Modification History:
 * Date           Author          Version            Description
 * ---------------------------------------------------------*
 * 2019/1/2      chenxu                     v1.0.0               初始创建
 */
@Data
public class WbsLotteryRecordVo extends WbsLotteryRecord {
    private String fkName;

    @JsonSerialize(using = BigDecimaleTransfer.class)
    private BigDecimal testMoney2;
    private BigDecimal testMoney3;
}

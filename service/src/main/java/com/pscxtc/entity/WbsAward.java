package com.pscxtc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * <p>
 * 奖品表
 * </p>
 *
 * @author com.pscxtc
 * @since 2019-01-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="WbsAward对象", description="奖品表")
public class WbsAward implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "奖品名称")
    @TableField("awardName")
    private String awardName;

    @ApiModelProperty(value = "奖品个数")
    @TableField("awardCount")
    private Integer awardCount;

    @ApiModelProperty(value = "奖品剩余个数")
    @TableField("residueCount")
    private Integer residueCount;

    @ApiModelProperty(value = "中奖人数")
    @TableField("winNum")
    private Integer winNum;

    @ApiModelProperty(value = "是否进行出奖(0:出奖,1:不出奖)")
    private Integer status;

    @ApiModelProperty(value = "员工是否参与抽奖(0:不参与,1参与)")
    @TableField("empStatus")
    private Integer empStatus;

    @ApiModelProperty(value = "一天最大中奖个数")
    @TableField("oneDayWinNum")
    private Integer oneDayWinNum;

    @ApiModelProperty(value = "中奖概率")
    private Double probability;

    @ApiModelProperty(value = "创建人")
    @TableField("createdBy")
    private Integer createdBy;

    @ApiModelProperty(value = "创建时间")
    @TableField("createTime")
    private Timestamp createTime;

    @ApiModelProperty(value = "修改人")
    @TableField("updatedBy")
    private Integer updatedBy;

    @ApiModelProperty(value = "修改时间")
    @TableField("updateTime")
    private Timestamp updateTime;

    @ApiModelProperty(value = "奖品等级（一等奖、二等奖...）")
    @TableField("awardLevel")
    private Integer awardLevel;

    @ApiModelProperty(value = "抽奖期数id")
    @TableField("lotteryId")
    private Integer lotteryId;

    @ApiModelProperty(value = "测试id")
    private Integer testId;


}

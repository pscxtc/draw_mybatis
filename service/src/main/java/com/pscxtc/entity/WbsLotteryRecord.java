package com.pscxtc.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import java.sql.Timestamp;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.pscxtc.utils.BigDecimaleTransfer;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 抽奖记录表
 * </p>
 *
 * @author com.pscxtc
 * @since 2019-01-02
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="WbsLotteryRecord对象", description="抽奖记录表")
public class WbsLotteryRecord implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "客户Id")
    @TableField("invId")
    private Integer invId;

    @ApiModelProperty(value = "客户姓名")
    @TableField("invName")
    private String invName;

    @ApiModelProperty(value = "客户手机号")
    @TableField("invMobile")
    private String invMobile;

    @ApiModelProperty(value = "客户地址")
    @TableField("invAddress")
    private String invAddress;

    @ApiModelProperty(value = "客户抽奖时间")
    @TableField("lotteryTime")
    private Timestamp lotteryTime;

    @ApiModelProperty(value = "客户抽奖状态(0:抽奖中,1:已中奖,2未中奖)")
    @TableField("lotteryStatus")
    private Integer lotteryStatus;

    @ApiModelProperty(value = "客户抽奖结果时间")
    @TableField("resultTime")
    private Timestamp resultTime;

    @ApiModelProperty(value = "客户抽奖结果(奖品名称)")
    @TableField("awardName")
    private String awardName;

    @ApiModelProperty(value = "奖品Id")
    @TableField("awardId")
    private Integer awardId;

    @ApiModelProperty(value = "创建人")
    @TableField("createdBy")
    private Integer createdBy;

    @ApiModelProperty(value = "创建时间")
    @TableField("createTime")
    private Timestamp createTime;

    @ApiModelProperty(value = "修改人")
    @TableField("updatedBy")
    private Integer updatedBy;

    @ApiModelProperty(value = "修改时间")
    @TableField("updateTime")
    private Timestamp updateTime;

    @ApiModelProperty(value = "是否填写过收货信息（0：未填写   1：填写）")
    @TableField("isWrite")
    private Integer isWrite;

    private Integer flag;

    @TableField("uuId")
    private String uuId;

    @ApiModelProperty(value = "测试金额")
    @TableField("testMoney")
    @JsonSerialize(using = BigDecimaleTransfer.class)
    private BigDecimal testMoney;


}

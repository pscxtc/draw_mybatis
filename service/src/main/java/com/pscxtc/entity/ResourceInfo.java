package com.pscxtc.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import java.sql.Timestamp;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="ResourceInfo对象", description="")
public class ResourceInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.ID_WORKER)
    private Integer id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "标识")
    private String key;

    @ApiModelProperty(value = "地址")
    private String url;

    @ApiModelProperty(value = "状态")
    @TableLogic
    private Integer state;

    @ApiModelProperty(value = "创建时间")
    @TableField("createDate")
    private Timestamp createDate;

    @ApiModelProperty(value = "创建人")
    private Integer creator;

    @ApiModelProperty(value = "修改时间")
    @TableField("updateDate")
    private Timestamp updateDate;

    @ApiModelProperty(value = "修改人")
    private Integer updater;


}

package com.pscxtc.entity;

import com.baomidou.mybatisplus.annotation.*;

import java.sql.Timestamp;
import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonValue;
import com.pscxtc.entity.enums.StatusEnum;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author pscxtc
 * @since 2019-01-15
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="RoleInfo对象", description="")
public class RoleInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主键")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "角色名")
    private String name;

    @ApiModelProperty(value = "状态")
    @TableLogic
    @TableField( fill = FieldFill.INSERT)
    private StatusEnum state;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "createDate",fill = FieldFill.INSERT)
    private Timestamp createDate;

    @ApiModelProperty(value = "创建人")
    @TableField( fill = FieldFill.INSERT)
    private Integer creator;

    @ApiModelProperty(value = "修改时间")
    @TableField("updateDate")
    private Timestamp updateDate;

    @ApiModelProperty(value = "修改人")
    private Integer updater;


}

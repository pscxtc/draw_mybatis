package com.pscxtc.client;

import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @Description:
 * @version: v1.0.0
 * @author: chenxu <br/><b>e-mail:</b><br/>com.pscxtc@163.com
 * @date: 2018/12/20 10:17
 * <p>
 * Modification History:
 * Date           Author          Version            Description
 * ---------------------------------------------------------*
 * 2018/12/20      chenxu                     v1.0.0               初始创建
 */
@Component
public class RabbitMQClient {
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 发送抽奖信息
     * @param message
     */
    public void send(String message) {
        rabbitTemplate.convertAndSend("lotteryDrawTest", message);
    }
}

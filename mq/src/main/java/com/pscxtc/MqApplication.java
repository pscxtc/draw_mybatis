package com.pscxtc;

import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;


@SpringBootApplication
public class MqApplication {


    @Bean
    public Queue testQueue(){
        return new Queue("lotteryDrawTest");
    }


	public static void main(String[] args) {
		SpringApplication.run(MqApplication.class, args);
	}

}


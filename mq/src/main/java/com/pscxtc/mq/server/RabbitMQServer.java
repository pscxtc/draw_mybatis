package com.pscxtc.mq.server;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.pscxtc.entity.WbsAward;
import com.pscxtc.entity.WbsLotteryRecord;
import com.pscxtc.service.IWbsLotteryRecordService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.List;

/**
 * @Description:
 * @version: v1.0.0
 * @author: chenxu <br/><b>e-mail:</b><br/>com.pscxtc@163.com
 * @date: 2018/12/20 10:20
 * <p>
 * Modification History:
 * Date           Author          Version            Description
 * ---------------------------------------------------------*
 * 2018/12/20      chenxu                     v1.0.0               初始创建
 */
@Component
@Transactional
public class RabbitMQServer {

    @Autowired
    @Qualifier("wbsLotteryRecordServiceImpl")
    private IWbsLotteryRecordService lotteryRecordService;

    @RabbitListener(queues = "lotteryDrawTest")
    public void receive(String message) {
        System.out.println("收到的 message 是：" + message);
        if (JSONUtil.isJson(message)) {
            JSONObject jsonObject = JSONUtil.parseObj(message);

//      抽奖
            List<WbsAward> wbsAwards =JSONUtil.parseArray(jsonObject.get("wbsAwards")).toList(WbsAward.class);
            WbsAward wbsAward = getPrizeIndex(wbsAwards);

//      转换抽奖记录
            WbsLotteryRecord lotteryRecord = new WbsLotteryRecord();
            lotteryRecord.setAwardId(wbsAward.getId());
            lotteryRecord.setAwardName(wbsAward.getAwardName());
            lotteryRecord.setCreatedBy(jsonObject.getInt("uId"));
            lotteryRecord.setCreateTime(new Timestamp(System.currentTimeMillis()));
            lotteryRecord.setUuId(jsonObject.getStr("uuId"));

            lotteryRecordService.save(lotteryRecord);
        }
    }



    /**
     * 根据Math.random()产生一个double型的随机数，判断每个奖品出现的概率
     * @param wbsAwards
     * @return random：奖品列表prizes中的序列（prizes中的第random个就是抽中的奖品）
     */
    public WbsAward getPrizeIndex(List<WbsAward> wbsAwards) {
        int random = -1;
        try{
            //计算总权重
            double sumWeight = 0;
            for (WbsAward p : wbsAwards) {
                sumWeight += p.getProbability();
            }

            //产生随机数
            double randomNumber;
            randomNumber = Math.random();

            //根据随机数在所有奖品分布的区域并确定所抽奖品
            double d1 = 0;
            double d2 = 0;
            for (int i = 0; i < wbsAwards.size(); i++) {
                d2 += Double.parseDouble(String.valueOf(wbsAwards.get(i).getProbability())) / sumWeight;
                if(i==0){
                    d1 = 0;
                }else{
                    d1 += Double.parseDouble(String.valueOf(wbsAwards.get(i - 1).getProbability())) / sumWeight;
                }
                if(randomNumber >= d1 && randomNumber <= d2){
                    random = i;
                    break;
                }
            }
        }catch(Exception e){
            System.out.println("生成抽奖随机数出错，出错原因：" +e.getMessage());
        }
        return wbsAwards.get(random);
    }

}
